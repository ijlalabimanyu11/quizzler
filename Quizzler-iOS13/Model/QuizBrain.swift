//
//  QuizBrain.swift
//  Quizzler-iOS13
//
//  Created by Imam 20/01/23.
//

import Foundation

struct logicKuis {
    
    var questionNumber = 0
    var score = 0

    let kuis = [
        question(q: "Negara yang masuk benua Asia", a:["Inggris", "Australia", "Singapore"], c:"Singapore"),
        question(q: "Pengendara Motor Menggunakan SIM", a: ["SIM A", "SIM B", "SIM C"], c:"SIM C"),
        question(q: "Ibu kota Provinsi Banten", a: ["Serang", "Bintaro", "Tangerang"], c:"Serang"),
        question(q: "Presiden ketiga Indonesia", a: ["Ir Soekarno","BJ. Habibie", "Gusdur"], c:"BJ. Habibie"),
        question(q: "Matahari terbit dari arah ", a: ["Barat", "Timur", "Utara"], c:"Timur"),
        question(q: "Jumlah pemain sepak bola tanpa kiper adalah", a: ["10", "11", "9"], c:"10"),
        question(q: "Olahraga yang menggunakan bola sebagai object, kecuali", a: ["Sepak takraw", "Basket", "Billiard"], c:"Billiard"),
        question(q: "Yang merupakan sayuran", a: ["Apel", "Burger", "Wortel"], c:"Wortel"),
        question(q: "Yang merupakan Integer", a: ["0.7", "hello world", "9"], c:"9"),
        question(q: "Besar pasak dari pada", a: ["Tiang", "Pengeluaran", "Saku"], c:"Tiang")

    ]

    func getQuestionText() -> String {
        return kuis[questionNumber].text
    }

    func getAnswers() -> [String] {
        return kuis[questionNumber].answer
    }
    
    func getProgress() -> Float {
        return Float(questionNumber) / Float(kuis.count)
    }
    
    mutating func getScore() -> Int {
        return score
    }
    
     mutating func nextQuestion() {
        
        if questionNumber + 1 < kuis.count {
            questionNumber += 1
        } else {
            questionNumber = 0
        }
    }
    
    mutating func checkAnswer(userAnswer: String) -> Bool {
        if userAnswer == kuis[questionNumber].jawabanBenar {
            score += 1
            return true
        } else {
            return false
        }
    }
}

