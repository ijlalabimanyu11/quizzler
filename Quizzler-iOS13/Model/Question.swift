//
//  QuizBrain.swift
//  Quizzler-iOS13
//
//  Created by Imam 20/01/23.
//


import Foundation

struct question{
    let text: String
    let answer: [String]
    let jawabanBenar: String
    
    init(q: String, a: [String], c: String) {
        text = q
        answer = a
        jawabanBenar = c
    }
    
}

    
